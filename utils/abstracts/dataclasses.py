from dataclasses import dataclass

from utils.abstracts.fields import ObjectUUID


@dataclass
class BaseDTO:
    id: ObjectUUID


@dataclass
class BaseCreateDTO:
    pass


@dataclass
class BaseUpdateDTO:
    pass
