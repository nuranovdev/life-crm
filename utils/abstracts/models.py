import uuid

from django.db import models


class BaseUuidModel(models.Model):
    id = models.UUIDField(
        primary_key=True,
        unique=True,
        default=uuid.uuid4,
        editable=False,
        verbose_name="ID",
    )

    class Meta:
        abstract = True


class BaseSlugModel(models.Model):
    name = models.CharField(max_length=256, verbose_name="Name")
    slug = models.SlugField(unique=True, blank=True, verbose_name="Slug")

    class Meta:
        abstract = True
