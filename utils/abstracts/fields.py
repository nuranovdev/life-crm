from typing import NewType
from uuid import UUID

ObjectUUID = NewType("uuid", UUID)
SLUG = NewType("slug", str)
