from abc import ABC, abstractmethod
from typing import Type
from django.db.models import Model

from utils.abstracts.dataclasses import BaseCreateDTO, BaseUpdateDTO, BaseDTO
from utils.abstracts.fields import ObjectUUID


class AbstractRepository(ABC):
    model: Type[Model] = Model

    @abstractmethod
    def create(self, obj: BaseCreateDTO) -> BaseDTO | Exception:
        pass

    @abstractmethod
    def get_by_id(self, object_id: ObjectUUID) -> BaseDTO | Exception:
        pass

    @abstractmethod
    def get_all(self) -> [BaseDTO]:
        pass

    @abstractmethod
    def delete(self, object_id: ObjectUUID) -> None | Exception:
        pass

    @abstractmethod
    def update(self, object_id: ObjectUUID, obj: BaseUpdateDTO) -> BaseDTO | Exception:
        pass

    def _build_dto(self, obj: model) -> BaseDTO:
        pass
