class RepositoryException(Exception):
    pass


class ServiceException(Exception):
    pass
