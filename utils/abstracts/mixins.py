from rest_framework.response import Response
from rest_framework import status


class CustomResponseMixin:

    def get_success_response(self, data: dict) -> Response:
        return Response(data=data, status=status.HTTP_200_OK)

    def get_error_response(self, message: str | Exception) -> Response:
        return Response(data={"message": str(message)}, status=status.HTTP_400_BAD_REQUEST)
