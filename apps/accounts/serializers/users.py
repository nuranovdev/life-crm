from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from apps.accounts.types.users import UserDTO, UserCreateDTO, UserUpdateDTO, UserAuthenticationDTO


class UserListSerializer(DataclassSerializer):
    class Meta:
        dataclass = UserDTO


class UserCreateSerializer(DataclassSerializer):
    class Meta:
        dataclass = UserCreateDTO


class UserUpdate(DataclassSerializer):
    class Meta:
        dataclass = UserUpdateDTO


class AuthenticationSerializer(DataclassSerializer):
    class Meta:
        dataclass = UserAuthenticationDTO
