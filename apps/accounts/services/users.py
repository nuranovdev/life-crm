from apps.accounts.repositories.users import UserRepository
from apps.accounts.types.users import UserDTO, UserUpdateDTO, UserCreateDTO
from utils.abstracts.exceptions import ServiceException


class UserServiceException:
    class Code:
        USER_EXISTS_WITH_SAME_USERNAME = "USER_EXISTS_WITH_SAME_USERNAME"
        USER_EXISTS_WITH_SAME_EMAIL = "USER_EXISTS_WITH_SAME_EMAIL"


class UserService:
    def __init__(self, config: dict, user_repository: UserRepository) -> None:
        self._config = config
        self._repository = user_repository

    def create(self, user_DTO: UserCreateDTO) -> UserDTO:
        user = self._repository.create(user_DTO)
        if isinstance(user, self._repository.model.IntegrityError):
            print(user)
            raise ServiceException("User already exist with same username or email")
        return user
