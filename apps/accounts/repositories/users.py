import logging

from dataclasses import asdict

from django.contrib.auth import get_user_model

from utils.abstracts.repositories import AbstractRepository
from utils.abstracts.fields import ObjectUUID
from utils.abstracts.exceptions import RepositoryException
from apps.accounts.types.users import UserCreateDTO, UserDTO, UserUpdateDTO, UserAuthenticationDTO

User = get_user_model()


class UserRepository(AbstractRepository):
    model = User

    def create(self, obj: UserCreateDTO) -> UserDTO | Exception:
        try:
            user = self.model.objects.create(
                username=obj.username,
                email=obj.email,
            )
            user.set_password(obj.password)
            user.save()
            return self._build_dto(user)
        except self.model.IntegrityError as er:
            return er

    def get_by_id(self, object_id: ObjectUUID) -> UserDTO | model.DoesNotExist:
        try:
            user = self.model.objects.get(id=object_id)
            return self._build_dto(user)
        except model.DoesNotExist as er:
            return RepositoryException(er)

    def get_all(self) -> [UserDTO]:
        users = self.model.objects.all()
        return list(map(lambda user: UserDTO(**asdict(user)), users))

    def update(self, object_id: ObjectUUID, obj: UserUpdateDTO) -> UserDTO | model.DoesNotExist:
        try:
            user = self.model.objects.get(id=object_id)
            user_data = asdict(obj)
            for key, value in user_data.items():
                setattr(user, key, value)
            user.save()
            return self._build_dto(user)
        except self.model.DoesNotExist as er:
            return RepositoryException(er)

    def delete(self, object_id: ObjectUUID) -> None | model.DoesNotExist:
        try:
            self.model.objects.get(id=object_id).delete()
        except self.model.DoesNotExist as er:
            return RepositoryException(er)

    def _build_dto(self, obj: User) -> UserDTO:
        return UserDTO(
            id=obj.id,
            username=obj.username,
            email=obj.email,
            image=obj.image,
            first_name=obj.first_name,
            last_name=obj.last_name,
            is_active=obj.is_active,
            date_joined=obj.date_joined,
            last_login=obj.last_login,
        )

    def get_by_username(self, username: str) -> UserDTO:
        try:
            user = self.model.objects.get(username=username)
            return self._build_dto(user)
        except self.model.DoesNotExist as er:
            return RepositoryException(er)

    def get_by_email(self, email: str) -> UserDTO:
        try:
            user = self.model.objects.get(email=email)
            return self._build_dto(user)
        except self.model.DoesNotExist as er:
            return RepositoryException(er)
