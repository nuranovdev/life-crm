from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from django.core.files.uploadedfile import InMemoryUploadedFile

from utils.abstracts.fields import UUID
from utils.abstracts.dataclasses import BaseDTO, BaseCreateDTO, BaseUpdateDTO


@dataclass
class UserDTO(BaseDTO):
    username: str
    email: str
    image: str | None
    first_name: str | None
    last_name: str | None
    is_active: bool
    date_joined: datetime
    last_login: datetime


@dataclass
class UserCreateDTO(BaseCreateDTO):
    username: str
    email: str
    password: str


@dataclass
class UserUpdateDTO(BaseUpdateDTO):
    username: Optional[str] | None
    email: Optional[str] | None
    password: Optional[str] | None
    image: Optional[InMemoryUploadedFile] | None
    first_name: str | None
    last_name: str | None
    is_active: bool | None


@dataclass
class UserAuthenticationDTO:
    email: str
    password: str
